<?php

    function calculateShippingThaipost( $data_product = array() ) {
        if ( count( ( array )$data_product ) < 1 ) {
            return array(
                'result' => false
                , 'message' => 'ไม่มีสินค้า'
            );
        }
        $wide = 0;
        $long = 0;
        $high = 0;
        $weight = 0;
        foreach ( $data_product['products'] as $key => $product ) {
            //รวมขนาดสินค้าทุกชิ้น
            $wide += $product['wide'];
            $long += $product['long'];
            if ( $high <= $product['high'] ) {
                $high = $product['high'];
            }
            $weight += $product['weight'];
        }

        $size_product = $wide + $long + $high;

        //แปลงกรัมเป็นกิโลกรัม
        $price_shipping = 0;
        if ( $weight > 10000 ) {
            return array(
                'result' => false
                , 'message' => 'พัสดุที่ให้บริการน้ำหนักเกิน 10 กิโลกรัม กรุณาตรวจเช็คสินค้า'
            );
        }

        switch ( $weight ) {
            case $weight <= 20 : $price_shipping = 32;
            break;
            case $weight > 20 && $weight <= 100 : $price_shipping = 37;
            break;
            case $weight > 100 && $weight <= 250 : $price_shipping = 42;
            break;
            case $weight > 250 && $weight <= 500 : $price_shipping = 52;
            break;
            case $weight > 500 && $weight <= 1000 : $price_shipping = 67;
            break;
            case $weight > 1000 && $weight <= 1500 : $price_shipping = 82;
            break;
            case $weight > 1500 && $weight <= 2000 : $price_shipping = 97;
            break;
            case $weight > 2000 && $weight <= 2500 : $price_shipping = 122;
            break;
            case $weight > 2500 && $weight <= 3000 : $price_shipping = 137;
            break;
            case $weight > 3000 && $weight <= 3500 : $price_shipping = 157;
            break;
            case $weight > 3500 && $weight <= 4000 : $price_shipping = 177;
            break;
            case $weight > 4000 && $weight <= 4500 : $price_shipping = 197;
            break;
            case $weight > 4500 && $weight <= 5000 : $price_shipping = 217;
            break;
            case $weight > 5000 && $weight <= 5500 : $price_shipping = 242;
            break;
            case $weight > 5500 && $weight <= 6000 : $price_shipping = 267;
            break;
            case $weight > 6000 && $weight <= 6500 : $price_shipping = 292;
            break;
            case $weight > 6500 && $weight <= 7000 : $price_shipping = 317;
            break;
            case $weight > 7000 && $weight <= 7500 : $price_shipping = 342;
            break;
            case $weight > 7500 && $weight <= 8000 : $price_shipping = 367;
            break;
            case $weight > 8000 && $weight <= 8500 : $price_shipping = 397;
            break;
            case $weight > 8500 && $weight <= 9000 : $price_shipping = 427;
            break;
            case $weight > 9000 && $weight <= 9500 : $price_shipping = 457;
            break;
            case $weight > 9500 && $weight <= 10000 : $price_shipping = 487;
            break;
        }

        $price_delivery = $price_shipping;
        if ( $size_product <= 40 ) {
            $price_delivery = $price_shipping + 9;
        } else if ( $size_product <= 51 ) {
            $price_delivery = $price_shipping + 12;
        } else if ( $size_product <= 61 ) {
            $price_delivery = $price_shipping + 16;
        } else if ( $size_product <= 71 ) {
            $price_delivery = $price_shipping + 20;
        } else if ( $size_product <= 81 ) {
            $price_delivery = $price_shipping + 25;
        } else {
            $price_delivery = $price_shipping + 32;
        }
        // if ( $wide <= 14 && $long <= 20 && $high <= 6 ) {
        //     $price_delivery = $price_shipping + 9;
        // } else if ( $wide <= 17 && $long <= 25 && $high <= 9 ) {
        //     $price_delivery = $price_shipping + 12;
        // } else if ( $wide <= 20 && $long <= 30 && $high <= 11 ) {
        //     $price_delivery = $price_shipping + 16;
        // } else if ( $wide <= 22 && $long <= 35 && $high <= 14 ) {
        //     $price_delivery = $price_shipping + 20;
        // } else if ( $wide <= 24 && $long <= 40 && $high <= 17 ) {
        //     $price_delivery = $price_shipping + 25;
        // } else if ( $wide <= 30 && $long <= 45 && $high <= 20 ) {
        //     $price_delivery = $price_shipping + 32;
        // }

        return array(
            'result' => true
            , 'message' => 'คำนวณสำเร็จ'
            , 'price_shipping' => $price_delivery
            , 'without_box' => $price_shipping
        );
    }

    function postRequestThaipost($url, $token, $content = null){

        $headers = [
            'Authorization: Token '. $token,
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($content) );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec( $ch );
        curl_close($ch);
        return json_decode($result, true);
    }

    function trackingOrderThaipost( $items ) {
        $api_token_url = 'https://trackapi.thailandpost.co.th/post/api/v1/authenticate/token';
        $api_track_url = 'https://trackapi.thailandpost.co.th/post/api/v1/track';
        $res_token = postRequestThaipost($api_token_url, thaipost_token_key);
        $res_items = postRequestThaipost($api_track_url, $res_token['token'], $items);
        return $res_items;  //ผลลัพธ์
    }
 
?>