<?php

/* common post funciton used for all web requests */

function postRequestKerry($url, $content = null){

    $headers = [
        'app_id: '.kerry_app_id,
        'app_key: '.kerry_app_key,
        'Content-Type: application/json; charset=UTF-8'
    ];

    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_URL, $url );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($content) );
    curl_setopt( $ch, CURLOPT_POST, true );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec( $ch );
    curl_close($ch);
    return json_decode($result, true);
}

function trackingOrderKerry( $data_arr ) {
    $tracking_api = productionKerry.'SmartEDI/shipment_info';
    $responseStr = postRequestKerry( $tracking_api, $data_arr );
    return $responseStr;
}

function calculateShippingKerry( $data_product = array() ) {
    // $data_product = array(
    //     'type_product' => 2

    //, 'delivery_zone' => 1
    //, 'delivery_province' => 1

    //, 'shipper_zone' => 2
    //, 'shipper_province' => 3

    //, 'products' => array(
    //         0 => array(
    //             'wide' => 5
    //, 'long' => 25
    //, 'high' => 5
    //, 'weight' => 5
    // )
    //, 1 => array(
    //             'wide' => 5
    //, 'long' => 5
    //, 'high' => 10
    //, 'weight' => 5
    // )
    // )
    // );
    if ( count( ( array )$data_product ) < 1 ) {
        return array(
            'result' => false
            , 'message' => 'ไม่มีสินค้า'
        );
    }
    $wide = 0;
    $long = 0;
    $high = 0;
    $weight = 0;
    foreach ( $data_product['products'] as $key => $product ) {
        //รวมขนาดสินค้าทุกชิ้น
        $wide += $product['wide'];
        $long += $product['long'];
        // $high += $product['high'];
        if ( $high <= $product['high'] ) {
            $high = $product['high'];
        }
        $weight += $product['weight'];
    }
    $size_product = $wide + $high;
    //คำนวนหาขนาดสินค้า เซ็นติเมตร
    $weight = $weight / 1000;
    //แปลงกรัมเป็นกิโลกรัม
    $price_shipping = 0;
    if ( $weight > 25 ) {
        return array(
            'result' => false
            , 'message' => 'พัสดุที่ให้บริการน้ำหนักเกิน 25 กิโลกรัม กรุณาตรวจเช็คสินค้า'
        );
    }
    if ( $wide > 200 ) {
        return array(
            'result' => false
            , 'message' => 'พัสดุที่ให้บริการเกิน 200 เซนติเมตร กรุณาตรวจเช็คสินค้า'
        );
    }
    $bangkok = $data_product['delivery_province'] = 1 ? true : false;
    //ในกทม = true ต่างจังหวัด = false
    if ( $size_product <= 55 && $weight <= 0.5 ) {
        $price_shipping = $bangkok ? 30 : 50;
    } else if ( $size_product <= 55 && $weight <= 1 ) {
        $price_shipping = $bangkok ? 40 : 60;
    } else if ( $size_product <= 64 && $weight <= 1  ) {
        $price_shipping = $bangkok ? 45 : 65;
    } else if ( $size_product <= 75 && $weight <= 7  ) {
        $price_shipping = $bangkok ? 65 : 80;
    } else if ( $wide <= 40 && $weight <= 2 ) {
        $price_shipping = $bangkok ? 35 + 5 : 55 + 5;
    } else if ( $wide <= 60 && $weight <= 7 ) {
        $price_shipping = $bangkok ? 65 + 10 : 80 + 10;
    } else if ( $wide <= 75 && $weight <= 7  ) {
        $price_shipping = $bangkok ? 80 + 15 : 90 + 15;
    } else if ( $wide <= 90 && $weight <= 10 ) {
        $price_shipping = $bangkok ? 90 + 20 : 100 + 20;
    } else if ( $wide <= 105 && $weight <= 15 ) {
        $price_shipping = $bangkok ? 130 + 25 : 145 + 25;
    } else if ( $wide <= 120 && $weight <= 15 ) {
        $price_shipping = $bangkok ? 185 + 30 : 205 + 30;
    } else if ( $wide <= 150 && $weight <= 20 ) {
        $price_shipping = $bangkok ? 290 : 330;
    } else if ( $wide <= 200 && $weight <= 25 ) {
        $price_shipping = $bangkok ? 380 : 420;
    }
    // if ( $size_product <= 55  && $weight <= 0.5 ) {
    //     $price_shipping = $bangkok ? 55 : 70;
    // } else if ( $size_product <= 55 && $weight <= 1 ) {
    //     $price_shipping = $bangkok ? 60 : 85;
    // } else if ( $size_product <= 64 && $weight <= 1  ) {
    //     $price_shipping = $bangkok ? 65 : 90;
    // } else if ( $size_product <= 75 && $weight <= 7  ) {
    //     $price_shipping = $bangkok ? 75 : 95;
    // } else if ( $wide <= 40 && $weight <= 2 ) {
    //     $price_shipping = $bangkok ? 55 + 5 : 70 + 5;
    // } else if ( $wide <= 60 && $weight <= 7 ) {
    //     $price_shipping = $bangkok ? 65 + 10 : 80 + 10;
    // } else if ( $wide <= 75 && $weight <= 7  ) {
    //     $price_shipping = $bangkok ? 80 + 15 : 90 + 15;
    // } else if ( $wide <= 90 && $weight <= 10 ) {
    //     $price_shipping = $bangkok ? 90 + 20 : 100 + 20;
    // } else if ( $wide <= 105 && $weight <= 15 ) {
    //     $price_shipping = $bangkok ? 130 + 25 : 145 + 25;
    // } else if ( $wide <= 120 && $weight <= 15 ) {
    //     $price_shipping = $bangkok ? 185 + 30 : 205 + 30;
    // } else if ( $wide <= 150 && $weight <= 20 ) {
    //     $price_shipping = $bangkok ? 290 : 330;
    // } else if ( $wide <= 200 && $weight <= 25 ) {
    //     $price_shipping = $bangkok ? 380 : 420;
    // }
    return array(
        'result' => true
        , 'message' => 'คำนวณสำเร็จ'
        , 'size_product' => $size_product
        , 'price_shipping' => $price_shipping
    );
}
 
?>