<?php

/* sign function */

function signParamScg( $str ) {
    return strtoupper( hash( 'sha256', $str ) );
}

/* build post param str */

function buildRequestParamScg( $data_arr ) {
    $sign = '';
    ksort( $data_arr );
    foreach ( $data_arr as $k => $v ) {
        if ( ( $v != null ) && ( $k != 'sign' ) ) {
            $sign .= $k.'='.$v.'&';
        }
    }
    $sign .= 'key=' . merchantPW;

    $data_arr['sign'] = signParamScg( $sign );

    $requestStr = '';
    foreach ( $data_arr as $k => $v )
    {
        $requestStr .= $k . '=' . urlencode( $v ) . '&';
    }
    return substr( $requestStr, 0, -1 );
}

/* common post funciton used for all web requests */

function postRequestScg( $url, $postData ) {
    $curl = curl_init ();
    $header[] = 'Content-type: application/x-www-form-urlencoded';
    $header[] = 'Accept: application/json';
    $header[] = 'Accept-Language: th';
    curl_setopt ( $curl, CURLOPT_URL, $url );
    curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false );
    // SSL certificate
    curl_setopt ( $curl, CURLOPT_SSL_VERIFYHOST, false );
    curl_setopt ( $curl, CURLOPT_HEADER, 0 );
    curl_setopt ( $curl, CURLOPT_HTTPHEADER, $header );
    curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt ( $curl, CURLOPT_POST, true );
    // post
    curl_setopt ( $curl, CURLOPT_POSTFIELDS, $postData );
    // post data
    curl_setopt ( $curl, CURLOPT_TIMEOUT, 10 );

    $responseText = curl_exec ( $curl );
    if ( curl_errno ( $curl ) ) {
        echo 'Errno' . curl_error ( $curl );
    }
    curl_close ( $curl );
    return $responseText;
}

function authenticationSCG() {
    $authen_api = developmentSCG.'/api/authentication';
    $authen_data = array(
        'username' => usernameSCG
        , 'password' => passwordSCG
    );
    $post_str = buildRequestParamScg( $authen_data );
    $responseStr = postRequestScg( $authen_api, $post_str );
    return json_decode( $responseStr, true );
}

function createOrderScg( $data_arr ) {
    $authen_resp = authenticationSCG();
    if ( $authen_resp['status'] == true ) {
        $createOrder_api = developmentSCG.'/api/orderwithouttrackingnumber';
        $data_arr['token'] = $authen_resp['token'];
        $post_str = buildRequestParamScg( $data_arr );
        $responseStr = postRequestScg( $createOrder_api, $post_str );
        return json_decode( $responseStr, true );
    }
    return $authen_resp;
}

function trackingOrderSCG( $tracking_no ) {
    $authen_resp = authenticationSCG();
    if ( $authen_resp['status'] == true ) {
        $tracking_api = developmentSCG.'/api/gethistoricalstatusbytrackingnumber';
        $data_arr = array(
            'token' => $authen_resp['token']
            , 'tracking_number' => $tracking_no
        );
        $post_str = buildRequestParamScg( $data_arr );
        $responseStr = postRequestScg( $tracking_api, $post_str );
        return json_decode( $responseStr, true );
    }
    return $authen_resp;
}

function queryPrePrintSCG( $tracking_no ) {
    $authen_resp = authenticationSCG();
    if ( $authen_resp['status'] == true ) {
        $tracking_api = developmentSCG.'/api/downloadwaybillbytrackingnumber';
        $data_arr = array(
            'token' => $authen_resp['token']
            , 'tracking_number' => array( $tracking_no )
        );
        $post_str = buildRequestParamSCG( $data_arr );
        return postRequestAndDownloadSCG( $tracking_api, $post_str, downloadDir );
    }
    return $authen_resp;
}

function postRequestAndDownloadSCG( $url, $postData, $saveDir ) {
    $curl = curl_init ();
    $header[] = 'Content-type: application/x-www-form-urlencoded';
    $header[] = 'Accept: application/json';
    $header[] = 'Accept-Language: th';
    curl_setopt ( $curl, CURLOPT_URL, $url );
    curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false );
    // SSL certificate
    curl_setopt ( $curl, CURLOPT_SSL_VERIFYHOST, false );
    curl_setopt ( $curl, CURLOPT_HEADER, 1 );
    curl_setopt ( $curl, CURLOPT_HTTPHEADER, $header );
    curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt ( $curl, CURLOPT_POST, true );
    // post
    curl_setopt ( $curl, CURLOPT_POSTFIELDS, $postData );
    // post data
    curl_setopt ( $curl, CURLOPT_TIMEOUT, 10 );
    curl_setopt ( $curl, CURLINFO_HEADER_OUT, true );

    $responseText = curl_exec ( $curl );
    if ( curl_errno ( $curl ) ) {
        echo 'Errno' . curl_error ( $curl );
    }
    curl_close ( $curl );

    list( $headers, $body ) = explode( '\r\n\r\n', $responseText, 2 );
    //1 ) process header
    $header_arr = array();
    $header_tmp = explode( '\n', $headers );
    foreach ( $header_tmp as $header_value ) {
        $pos = strpos( $header_value, ':' );
        $k = trim( substr( $header_value, 0, $pos ) );
        $v = trim( substr( $header_value, $pos+1 ) );
        if ( !empty( $k ) )
        $header_arr[$k] = $v;
    }
    $file_name = $header_arr['Content-Disposition'];
    $file_type = $header_arr['Content-Type'];
    $file_save_name = substr( $file_name, strrpos( $file_name, '=' )+1 );

    //2 ) process body
    $file_content = $body;
    $filename  = $saveDir . $file_save_name;
    if ( is_writable( $saveDir ) ) {
        if ( !$handle  =  fopen( $filename, 'w' ) ) {
            echo  "cannot open  $filename \n" ;
            exit;
        }

        if ( fwrite( $handle,  $file_content ) ===  FALSE ) {
            echo  "cannot write file  $filename\n" ;
            exit;
        }

        echo  "write $filename success\n" ;
        fclose( $handle );
        return true;
    } else {
        echo  "file $filename not writable\n" ;
    }
    return false;
}

function calculateShippingSCG( $data_product = array() ) {
    // $data_product = array(
    //     'type_product' => 2

    //     , 'delivery_zone' => 1
    //     , 'delivery_province' => 1

    //     , 'shipper_zone' => 2
    //     , 'shipper_province' => 3

    //     , 'products' => array(
    //         0 => array(
    //             'wide' => 5
    //             , 'long' => 25
    //             , 'high' => 5
    //             , 'weight' => 5
    //         )
    //         , 1 => array(
    //             'wide' => 5
    //             , 'long' => 5
    //             , 'high' => 10
    //             , 'weight' => 5
    //         )
    //     )
    // );
    if ( count( ( array )$data_product ) < 1 ) {
        return array(
            'result' => false
            , 'message' => 'ไม่มีสินค้า'
        );
    }
    $wide = 0;
    $long = 0;
    $high = 0;
    $weight = 0;
    foreach ( $data_product['products'] as $key => $product ) {
        //รวมขนาดสินค้าทุกชิ้น
        $wide += $product['wide'];
        $long += $product['long'];
        $high += $product['high'];
        $weight += $product['weight'];
    }
    $size_product = $wide + $long + $high;
    //คำนวนหาขนาดสินค้า เซ็นติเมตร
    $price_shipping = 0;
    $weight = $weight / 1000;
    //แปลงกรัมเป็นกิโลกรัม
    if ( $data_product['type_product'] == 1 ) {
        //พัสดุธรรมดา
        if ( $weight > 25 ) {
            return array(
                'result' => false
                , 'message' => 'พัสดุที่ให้บริการน้ำหนักเกิน 25 กิโลกรัม กรุณาตรวจเช็คสินค้า'
            );
        }
        if ( $size_product > 200 ) {
            return array(
                'result' => false
                , 'message' => 'พัสดุที่ให้บริการเกิน 200 เซนติเมตร กรุณาตรวจเช็คสินค้า'
            );
        }
        $zone = $data_product['shipper_zone'] == $data_product['delivery_zone'] ? true : false;
        //ในภาค = true ส่งข้ามภาค = false
        if ( $wide <= 29.7 && $long <= 42 && $high <= 10 ) {
            //ซองเอกสาร ขนาดน้อยกว่าหรือเท่ากับA3
            $price_shipping = $zone ? 40 + 5 : 50 + 5;
            //5บาทค่าซอง
        } else if ( ( $wide > 29.7 && $wide <= 36 ) && $long <= 28 && $high <= 10 ) {
            $price_shipping = $zone ? 40 : 60;
        } else if ( ( $wide > 36 && $wide <= 44 ) && ( $long > 28 && $long <= 31 ) && $high <= 10 ) {
            $price_shipping = $zone ? 60 : 75;
        } else if ( $size_product <= 40 ) {
            $price_shipping = $zone ? 40 + 5 : 50 + 5;
        } else if ( $size_product <= 50 ) {
            $price_shipping = $zone ? 55 : 70;
        } else if ( $size_product <= 60 ) {
            $price_shipping = $zone ? 70 + 15 : 85 + 15;
        } else if ( $size_product <= 80 ) {
            $price_shipping = $zone ? 90 + 25 : 105 + 25;
        } else if ( $size_product <= 100 ) {
            $price_shipping = $zone ? 130 + 25 : 145 + 25;
        } else if ( $size_product <= 120 ) {
            $price_shipping = $zone ? 180 + 30 : 210 + 30;
        } else if ( $size_product <= 140 ) {
            $price_shipping = $zone ? 240 + 40 : 240 + 40;
        } else if ( $size_product <= 160 ) {
            $price_shipping = $zone ? 300 : 330;
        } else if ( $size_product <= 180 ) {
            $price_shipping = $zone ? 350 : 380;
        } else if ( $size_product <= 200 ) {
            $price_shipping = $zone ? 380 : 420;
        }
    } else if ( $data_product['type_product'] == 2 ) {
        // พัสดุแช่แข็ง
        $perimeter = array( 1, 2, 3, 4 );
        //ปริมณฑล
        if ( $weight > 20 ) {
            return array(
                'result' => false
                , 'message' => 'พัสดุที่ให้บริการน้ำหนักเกิน 20 กิโลกรัม กรุณาตรวจเช็คสินค้า'
            );
        }
        if ( $size_product > 120 ) {
            return array(
                'result' => false
                , 'message' => 'พัสดุที่ให้บริการเกิน 120 เซนติเมตร กรุณาตรวจเช็คสินค้า'
            );
        }
        if ( in_array( $data_product['delivery_province'], $perimeter ) || $data_product['shipper_province'] == $data_product['delivery_province'] ) {
            //ภายในกรุงเทพฯ / ปริมณฑลและภายในจังหวัดเดียวกัน
            $type = 1;
        } else if ( in_array( $data_product['shipper_province'], $perimeter ) && !in_array( $data_product['delivery_province'], $perimeter ) ) {
            $type = 2;
        } else if ( $data_product['shipper_province'] != 1 && $data_product['delivery_province'] != 1 ) {
            $type = 3;
        }

        if ( $size_product <= 40 ) {
            $price_shipping = $type == 1 ? 140 : $type == 2 ? 190 :  230;
        } else if ( $size_product <= 60 ) {
            $price_shipping = $type == 1 ? 160 : $type == 2 ? 210 :  250;
        } else if ( $size_product <= 80 ) {
            $price_shipping = $type == 1 ? 190 : $type == 2 ? 240 :  300;
        } else if ( $size_product <= 100 ) {
            $price_shipping = $type == 1 ? 220 : $type == 2 ? 270 :  350;
        }
    }
    return array(
        'result' => true
        , 'message' => 'คำนวณสำเร็จ'
        , 'size_product' => $size_product
        , 'price_shipping' => $price_shipping
    );
}
 
?>